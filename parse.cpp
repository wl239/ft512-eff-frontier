#include <cstdlib>
#include <cstdio>
#include <iostream>
#include <fstream>
#include <Eigen/Dense>

using namespace Eigen;
using namespace std;

bool checkFile(char * args){
  ifstream file(args);
  if(!file){
    file.close();
    return false;
  }
  char ch;
  file>>ch;
  if(file.eof()){
    file.close();
    return false;
  }
  file.close();
  return true;
}

vector<vector<string> > readCSV(char * filename){
  ifstream file(filename);
  string line, str;
  vector<string> row;
  vector<vector<string> > lines;
  while(getline(file, line)){
    stringstream s(line);
    while(getline(s,str,',')){
      row.push_back(str);
    }
    lines.push_back(row);
    row.clear();
  }
  return lines;
}

bool isDouble(string & str){
  for(size_t i = 0; i < str.length(); i++){
    if(isalpha(str[i])){
      return false;
    }
  }
  if(!atof(str.c_str())){
    return false;
  }
  return true;
}

vector<vector<double> > getCorr(vector<vector<string> > & l){
  int n = l.size();
  int m = l[0].size();
  vector<vector<double> > data(n,vector<double>(m,0));
  for(int i = 0; i < n; i++){
    for(int j = 0; j < n; j++){
      data[i][j] = atof(l[i][j].c_str());
    }
  }
  return data;
}

