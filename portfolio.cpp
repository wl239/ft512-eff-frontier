#include <cstdlib> //EXIT_SUCCESS
#include <Eigen/Dense> //Eigen
#include <vector>

using namespace Eigen;
using namespace std;

size_t checkNoNeg(size_t n, VectorXd & weight, vector<size_t> & v){
  size_t num = 0;
  for(size_t i = 0; i < n; i++){
    if(weight(i) < -0.0000){
      num++;
      v.push_back(i);
    }
  }
  return num;
}

VectorXd portWeight(size_t n, double & rp, MatrixXd assets, MatrixXd cov, int rest){
  MatrixXd a1 = MatrixXd::Ones(1, n);
  MatrixXd a2(1, n);
  for(size_t i = 0; i < n; i++){
    a2(i) = assets(i,0);
  }
  MatrixXd a(2,n);
  a << a1, a2;
  MatrixXd b1 = MatrixXd::Zero(n,1);
  MatrixXd B(n+2,1);
  B << b1, 1, rp;
  MatrixXd O = MatrixXd::Zero(2,2);
  MatrixXd A(n+2, n+2);
  A << cov, a.transpose(), a, O;
  VectorXd W1(n+2,1);
  W1 = A.fullPivLu().solve(B);
  if(rest == 0){
    return W1.head(n);
  }
  
  VectorXd W2(n+2,1);
  vector<size_t> index;
  size_t neg = checkNoNeg(n, W1, index);
  if(neg == 0){
    return W1.head(n);
  }
  size_t delta_neg;
  do{
    MatrixXd R = MatrixXd::Zero(neg,n+2);
    MatrixXd T = MatrixXd::Zero(neg,1);
    for(size_t i = 0; i < neg; i++){
      R(i,index[i]) = 1;
    }
    MatrixXd OO = MatrixXd::Zero(neg,neg);
    MatrixXd AA(n+2+neg,n+2+neg);
    AA<<A,R.transpose(),R,OO;
    MatrixXd BB(n+2+neg,1);
    BB<<B,T;
    W2.resize(n+2+neg,1);
    W2 = AA.fullPivLu().solve(BB);
    delta_neg = checkNoNeg(n,W2,index);
    neg += delta_neg;
  }while(delta_neg != 0);
  return W2.head(n);
}

double calROR(size_t n, VectorXd weights, MatrixXd assets){
  double ror = 0;
  for(size_t i = 0; i < n; i++){
    ror += weights(i)*assets(i,0);
  }
  return ror;
}

double calSD(size_t n, VectorXd weights, MatrixXd assets, MatrixXd cov){
  double sum = 0;
  for(size_t i = 0; i < n; i++){
    for(size_t j = 0; j < n; j++){
      sum += weights(i)*weights(j)*cov(i,j);
    }
  }
  double sd = sqrt(sum);
  return sd;
}
