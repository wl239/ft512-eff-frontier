#include <cstdlib> //EXIT_SUCCESS
#include <iostream> //std::cin std::cout std::cerr
#include <vector> //std::vector
#include "parse.cpp"
#include <Eigen/Dense> //Eigen
#include <unistd.h> //getopt
#include <iomanip> //setprecision
#include "portfolio.cpp"

using namespace Eigen;
using namespace std;

int main(int argc, char** argv){
  if(argc != 3 && argc != 4){
    cerr << "Invalid no. of arguments" << endl;
    return EXIT_FAILURE;
  }
  
  if(argc == 4){
    if(strcmp(argv[1],"-r") != 0 && strcmp(argv[2],"-r") != 0 && strcmp(argv[3],"-r") != 0){
      cerr << "Invalid additional constraint" << endl;
      return EXIT_FAILURE;
    }
  }
  
  int option;
  int restrict = 0; // no restrict
  opterr = 0;
  while((option = getopt(argc, argv, "r")) != -1){
    switch(option){
    case 'r':
      restrict = 1; // non-negative restrict
      break;
    case '?':
      cerr << "Invalid option: strange" << endl;
      return EXIT_FAILURE;
    }
  }
  
  vector<vector<string> > str_universe;
  vector<vector<string> > str_correlation;
  
  if(restrict == 0){
    if(checkFile(argv[1]) == false || checkFile(argv[2]) == false){
      cerr << "Invalid input file" << endl;
      return EXIT_FAILURE;
    }
    str_universe = readCSV(argv[1]);
    str_correlation = readCSV(argv[2]);
  }
  else if(restrict == 1){
    if(checkFile(argv[2]) == false || checkFile(argv[3]) == false){
      cerr << "Invalid input file" << endl;
      return EXIT_FAILURE;
    }
    str_universe = readCSV(argv[2]);
    str_correlation = readCSV(argv[3]);
  }
  
  //test no. of universe and no. of correlation
  if(str_universe.size() != str_correlation.size()){
    cerr << "No. of rows in universe and in correlation are different" << endl;
    return EXIT_FAILURE;
  }
  size_t n_asset = str_universe.size();
  
  //test no. of columns in universe and correlation file
  //test data feature in universe and correlation file
  for(size_t i = 0; i < n_asset; i++){
    if(str_universe[i].size() != 3){
      cerr << "Invalid universe file: contain different columns." << endl;
      return EXIT_FAILURE;
    }
    if(!isDouble(str_universe[i][1]) || !isDouble(str_universe[i][2])){
      cerr << "Invalid universe data: contains the letter" << endl;
      return EXIT_FAILURE;
    }
    if(str_correlation[i].size() != n_asset){
      cerr << "Invalid correlation file: contain different columns." << endl;
      return EXIT_FAILURE;
    }
    for(size_t j = 0; j < n_asset; j++){
      if(!isDouble(str_correlation[i][j])){
    cerr << "Invalid correlation file: cannot turn to double." << endl;
    return EXIT_FAILURE;
      }
    }
  }
  
  MatrixXd univ(n_asset,2);
  for(size_t i = 0; i < n_asset; i++){
    univ(i,0) = atof(str_universe[i][1].c_str());
    univ(i,1) = atof(str_universe[i][2].c_str());
  }
  
  vector<vector<double> > correlation = getCorr(str_correlation);
  MatrixXd cov(n_asset, n_asset);
  for(size_t i = 0; i < n_asset; i++){
    for(size_t j = 0; j < n_asset; j++){
      if(fabs(correlation[i][j]) > 1.001){
    cerr << "Invalid correlation file: not in (-1,1)" << endl;
    return EXIT_FAILURE;
      }
      if(i == j){
        if(fabs(correlation[i][j]) > 1.00){
          cerr << "Invalid correlation file: at (i,i), not equal to 1 or -1" << endl;
          return EXIT_FAILURE;
        }
      }
      if(fabs(correlation[i][j] - correlation[j][i]) > 0.0){
    cerr << "Invlid correlation file: (i,j) not equal (j,i)" << endl;
    return EXIT_FAILURE;
      }
      cov(i,j) = univ(i,1) * correlation[i][j] * univ(j,1);
    }
  }
  
  cout << "ROR,volatility" << endl;
  double rp = 0;
  for(size_t i = 0; i < 26; i++){
    rp+=0.01;
    VectorXd weight = portWeight(n_asset, rp, univ, cov, restrict);
    double sd = calSD(n_asset, weight, univ, cov);
    cout << fixed << setprecision(1) << rp*100 << "%,";
    cout << fixed << setprecision(2) << 100*sd << "%" << endl;
  }
  
  return EXIT_SUCCESS;
}

