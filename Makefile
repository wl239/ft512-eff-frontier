CCFLAGS=--std=gnu++98 -pedantic -Wall -Werror -ggdb3
efficient_frontier: main.cpp parse.cpp portfolio.cpp
	g++ -o efficient_frontier $(CCFLAGS) main.cpp
clean:
	rm -f *~ efficient_frontier
